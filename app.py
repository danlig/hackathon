import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.graph_objs as go
import plotly.express as px
import pandas as pd

url = 'https://raw.githubusercontent.com/datasets/covid-19/master/data/countries-aggregated.csv'
url_2 = 'https://raw.githubusercontent.com/datasets/covid-19/master/data/reference.csv'

df = pd.read_csv(url)
df2 = pd.read_csv(url_2, usecols=['iso3', 'Country_Region'])
df_geo = pd.merge(df, df2, left_on='Country', right_on='Country_Region', how='left')
df_geo = df_geo.drop('Country_Region', axis=1)

countries = df['Country'].unique()
df['Date_num'] = df['Date'].str.replace("-", '').astype('int32')

app = dash.Dash(__name__)
server = app.server

app.layout = html.Div([
    html.H2("All information of Country"),
    html.Div(
        [
            dcc.Dropdown(
                id="drop-1",
                options=[{
                    'label': i,
                    'value': i
                } for i in countries],
                value='Poland')
        ],
        style={'width': '25%',
               'display': 'inline-block'}),
    dcc.Graph(id='scatter-plot'),
    html.Div(
        [
            dcc.RangeSlider(
                id='range-slider',
                min=df['Date_num'].min(),
                max=df['Date_num'].max(),
                value=[df['Date_num'].min(), df['Date_num'].max()],
                marks={20200122: '2020-01-22',
                       20200331: '2020-03-31',
                       20200630: '2020-06-30',
                       20200930: '2020-09-30',
                       20201217: '2020-12-17'}),
        ]
    ),
    html.H2("Compare two Country"),
    html.Div(
        [
            dcc.Dropdown(
                id="drop-2",
                options=[{
                    'label': i,
                    'value': i
                } for i in countries],
                value='Poland'),
        ],
        style={'width': '25%',
               'display': 'inline-block'}),
    html.Div(
        [
            dcc.Dropdown(
                id="drop-3",
                options=[{
                    'label': i,
                    'value': i
                } for i in countries],
                value='Angola'),
        ],
        style={'width': '25%',
               'display': 'inline-block'}),
    html.Div(
        [
            dcc.Dropdown(
                id="drop-4",
                options=[
                    {'label': 'Confirmed', 'value': 'Confirmed'},
                    {'label': 'Recovered', 'value': 'Recovered'},
                    {'label': 'Deaths', 'value': 'Deaths'}
                ],
                value='Confirmed'),
        ],
        style={'width': '25%',
               'display': 'inline-block'}),
    dcc.Graph(id='scatter-plot-2'),
    html.H2("Daily information"),
    html.Div(
        [
            dcc.Dropdown(
                id="drop-5",
                options=[{
                    'label': i,
                    'value': i
                } for i in countries],
                value='Poland'),
        ],
        style={'width': '25%',
               'display': 'inline-block'}),
    html.Div(
        [
            dcc.Dropdown(
                id="drop-6",
                options=[
                    {'label': 'Confirmed', 'value': 'Confirmed'},
                    {'label': 'Recovered', 'value': 'Recovered'},
                    {'label': 'Deaths', 'value': 'Deaths'}
                ],
                value='Confirmed'),
        ],
        style={'width': '25%',
               'display': 'inline-block'}),
    dcc.Graph(id='scatter-plot-3'),
    html.H2("Geo information"),
    html.Div(
        [
            dcc.DatePickerSingle(
                id='date-picker',
                min_date_allowed='2020-01-22',
                max_date_allowed='2020-12-17',
                date='2020-12-17',
                display_format='MMM Do, YY'),
        ],
        style={'width': '12.5%',
               'display': 'inline-block'}),
    html.Div(
        [
            dcc.Dropdown(
                id="drop-7",
                options=[
                    {'label': 'Confirmed', 'value': 'Confirmed'},
                    {'label': 'Recovered', 'value': 'Recovered'},
                    {'label': 'Deaths', 'value': 'Deaths'}
                ],
                value='Confirmed'),
        ],
        style={'width': '25%',
               'display': 'inline-block'}),
    dcc.Graph(id='geo-plot'),
])


@app.callback(
    Output('scatter-plot', 'figure'),
    Input('drop-1', 'value'),
    Input('range-slider', 'value'))
def update_scatter(country, selected_range):
    r_min, r_max = selected_range
    df_country = df[df['Country'] == country]
    df_filtered = df_country[(df_country['Date_num'] <= r_max) & (df_country['Date_num'] >= r_min)]
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=df_filtered['Date'], y=df_filtered['Confirmed'], mode='markers',
                             name='Confirmed'))
    fig.add_trace(go.Scatter(x=df_filtered['Date'], y=df_filtered['Recovered'], mode='markers',
                             name='Recovered'))
    fig.add_trace(go.Scatter(x=df_filtered['Date'], y=df_filtered['Deaths'],  mode='markers',
                             name='Deaths'))
    return fig


@app.callback(
    Output('scatter-plot-2', 'figure'),
    Input('drop-2', 'value'),
    Input('drop-3', 'value'),
    Input('drop-4', 'value'))
def update_scatter2(country1, country2, stat):
    df_filtered = df[(df['Country'] == country1) | (df['Country'] == country2)]
    fig = px.scatter(df_filtered, x='Date', y=stat, color='Country')
    return fig


@app.callback(
    Output('scatter-plot-3', 'figure'),
    Input('drop-5', 'value'),
    Input('drop-6', 'value'))
def update_scatter3(country, stat):
    df_filtered = df[df['Country'] == country]
    col = stat + '_diff'
    df_filtered[col] = df_filtered[stat].diff()
    fig = px.scatter(df_filtered, x='Date', y=col)
    return fig


@app.callback(
    Output('geo-plot', 'figure'),
    Input('date-picker', 'date'),
    Input('drop-7', 'value'),)
def update_geo(dat, stat):
    df_filtered = df_geo[df_geo['Date'] == dat]
    fig = px.scatter_geo(df_filtered, locations='iso3', size=stat, hover_name='Country',
                         color='Country', height=700)
    return fig


if __name__ == '__main__':
    app.run_server(debug=True)
